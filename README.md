# Notepad

Simple notepad application to explore working with ReactJS.

## Install Locally

__Clone this repository:__

`git clone https://komplexb@bitbucket.org/komplexb/notepad.git`

__Install the dependencies:__

`npm install` (Requires Node.js)

__Development mode with livereload:__

`gulp watch`

__Build__

`gulp build`

## Tools used:
- [React Starterify](https://github.com/Granze/react-starterify) is a succinct boilerplate that uses Gulp and Browserify to get React development going quickly.  
- [Topcoat](http://topcoat.io) for an app like skin.