'use strict';

var _ = require('lodash');
var React = require('react');
window.React = React;

var Header = require('./components/Header.jsx');
var Notepad = require('./components/Notepad.jsx');
var Storage = require('./Storage');


var notepad = {
  notes:
  [
      {
          id: 1,
          content: "Notepad Source\n\nSimple notepad application to explore working with ReactJS.\nhttps://bitbucket.org/komplexb/notepad"
      },
      {
          id: 2,
          content: "knackered\nexhausted; very tired: He is really knackered after work.\n\nhttp://dictionary.reference.com/wordoftheday/2015/03/15/knackered"
      },
      {
          id: 3,
          content: "momism\nexcessive adulation of the mother and undue dependence on maternal care or protection, resulting in absence or loss of maturity and independence.\n\nhttp://dictionary.reference.com/wordoftheday/2015/03/11/momism"
      },
      {
          id: 4,
          content: "Monomaniacal focus\n\nMonomaniacal focus on a single goal is perhaps the ultimate success stratagem."
      },
      {
          id: 5,
          content: "Father\nByron the Elder"
      },
      {
          id: 6,
          content: "Son\nByron the Younger"
      },
      {
          id: 7,
          content: "This is a really long title to test overflow behavior\nHello, world!\nBoring.\nBoring."
      }
  ],
  selectedId: 1
};

var nextNodeId = 1;

var onAddNote = function() {
  var note = {id: Date.now(), content: ''};
  nextNodeId++;
  notepad.notes.unshift(note);
  notepad.selectedId = note.id;
  onChange();
};

var onChangeNote = function(id, value) {
  var note = _.find(notepad.notes, function(note) {
    return note.id === id;
  });
  if(note) {
    note.content = value;
  }
  onChange();
}

var onSelectNote = function(id) {
  notepad.selectedId = id;
  onChange();
};

var onDeleteNote = function(id) {
  var note = _.find(notepad.notes, function(note) {
    return note.id === id;
  });
  if(note) {
    //The filter() method creates a new array with all elements that pass the test implemented by the provided function.
    notepad.notes = notepad.notes.filter(function(note) {
      return note.id !== id; //the test performs the "delete" by returning an array list that excludes the entry with the value of "id"
    });
  }

  // having deleted the note lets show the first note in the list
  if(notepad.notes.length > 0)
    onSelectNote(_.first(notepad.notes).id);
  else {
    onAddNote();
    onChangeNote(notepad.selectedId, "Write your first note!");
  }

  onChange();
};

var onSearchNotes = function(str) {
  var searchResults = notepad.notes.map(function(note) {
    if(note.content.toLowerCase().search(str.toLowerCase()) >= 0) {
      return note;
    }
  });

  return searchResults;
};

var onChange = function() {
  React.render(
    <Notepad
    notepad={notepad}
    onSelectNote={onSelectNote}
    onDeleteNote={onDeleteNote}
    onChangeNote={onChangeNote}
    onSearchNotes={onSearchNotes}
    />, document.getElementById("notepad")
  );
};

onChange();

React.render(
  <Header onAddNote={onAddNote} />, document.getElementById("app-header")
);
