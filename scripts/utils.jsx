var updateAppModeMixin = {
  updateAppMode: function(mode) {
    var body = document.getElementsByTagName("body");
    body[0].setAttribute("data-app-mode", mode);
  }
}

module.exports = updateAppModeMixin;
