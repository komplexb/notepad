'use strict';

var React = require('react');

var NoteEditor = React.createClass({

  onChange: function(event) {
    this.props.onChange(this.props.note.id, event.target.value);
  },

  toggleAutoFocus: function() {
    // no autofocus on $portables: 1023px
    if (window.matchMedia("(max-width: 1023px)").matches) {
      return "autofocus";
    } else {
      return "";
    }
  },

  render: function() {
    return (
        <div>
          <textarea className="note-body topcoat-textarea full x1-top" rows="" cols="" value={this.props.note.content} onChange={this.onChange} placeholder="Enter notes here"></textarea>
        </div>
    );
  }
});

module.exports = NoteEditor;
