'use strict';

var React = require('react');
var NoteSummary = require('./NoteSummary.jsx');
var utils = require('../utils.jsx');

var DeleteDialog = React.createClass({
  render: function() {
    var styles = { color: '#288edf' };

    return (
      <div className="note-list-item__delete-note">
        <button
          title="Delete this note."
          key={this.props.noteId}
          onClick={this.props.onDelete}
          className="topcoat-icon-button--quiet"
          disabled={this.props.disableDelete ? "disabled" : ""}
        >
          {
            this.props.toggleDeleteDialogs ?
            <span className="topcoat-icon icomatic icon" style={styles}>checkmark</span> :
            <span className="topcoat-icon icomatic icon" style={styles}>delete</span>
          }
        </button>
          {
            this.props.toggleDeleteDialogs ?
            <button title="Don't delete this note." className="topcoat-icon-button--quiet" onClick={this.props.onCancel}>
              <span className="topcoat-icon icomatic icon" style={styles}>cancel</span>                   </button> : null
          }
      </div>
    );
  }
});

var NotesList = React.createClass({
  getInitialState: function() {
    return {
      isConfirming: false,
      isSearching: false,
      searchString: "",
      searchResults: [],
    };
  },

  searchNotes: function(event) {
    this.setState({
      isSearching: event.target.value.length === 0 ? false : true,
      searchString: event.target.value,
      searchResults: this.props.onSearch(event.target.value)
    });
  },

  cancelSearch: function() {
    this.setState({ isSearching: false, searchString: '', searchResults: [] });
  },

  deleteThisNote: function(noteId, event) {
    event.stopPropagation();
    this.props.onSelectNote(noteId);

    if(this.state.isConfirming) {
      this.props.onDelete(noteId);

      // refresh search results list as well
      if(this.state.isSearching) {
        this.setState({searchResults: this.props.onSearch(this.state.searchString)});
      }

      this.setState({isConfirming: false});
    }
    else {
      this.setState({isConfirming: true});
    }
  },

  onCancelDelete: function(event) {
    event.stopPropagation();
    this.setState({ isConfirming: false });
  },

  clickNote: function(noteId) {
    utils.updateAppMode("editing");
    this.props.onSelectNote(noteId);
  },

  renderNotes: function(notepad) {
    var notes = (this.state.isSearching === false || this.state.searchString.length === 0) ? notepad.notes : this.state.searchResults;

    return notes.map(function (note) {
      if(note !== undefined) {
        var toggleDeleteDialogs = this.state.isConfirming && note.id === notepad.selectedId;
        var disableDelete = this.state.isConfirming && note.id !== notepad.selectedId;

        return (
          <li data-is-active={note.id === notepad.selectedId} key={note.id} onClick={this.clickNote.bind(null, note.id)} className="note-list-item topcoat-list__item">
            <NoteSummary note={note}></NoteSummary>

            <DeleteDialog
              toggleDeleteDialogs={toggleDeleteDialogs}
              disableDelete={disableDelete}
              onDelete={this.deleteThisNote.bind(null, note.id)}
              onCancel={this.onCancelDelete}/>

          </li>

        );
      }
    }.bind(this))
  },

  render: function() {
    var notepad = this.props.notepad;

    return (
      <div className="note-list-wrap">
        <div className="search-widget">
          <input
            onChange={this.searchNotes}
            value={this.state.searchString}
            type="search"
            placeholder="Search notes"
            className="search-notes topcoat-search-input full x1 x1-top"
          />
          <span data-is-visible={this.state.isSearching} onClick={this.cancelSearch} className="cancel-search topcoat-icon icomatic icon">cancel</span>
        </div>

        <div className="note-list topcoat-list x1-top">
          <h3 className="topcoat-list__header">{this.state.isSearching ? "Search Results" : "All Notes"}</h3>
          <ul className="topcoat-list__container">
            {this.renderNotes(notepad)}
          </ul>
        </div>
      </div>
    );
  }
});

module.exports = NotesList;
