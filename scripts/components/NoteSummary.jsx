'use strict';

var React = require('react');

var NoteSummary = React.createClass({
  render: function() {
    var note = this.props.note;
    var title = note.content.substring(0, note.content.indexOf("\n"));
    title = title || note.content;
    var styles = { backgroundColor: "#A5A7A7" };
    return (
        <a href="#note" key={note.id} className="note-link">{title}</a>
    );
  }
});

module.exports = NoteSummary;
