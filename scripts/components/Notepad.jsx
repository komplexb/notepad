'use strict';

var _ = require('lodash');
var React = require('react');
window.React = React;

var NotesList = require('./NotesList.jsx');
var NoteEditor = require('./NoteEditor.jsx');

var Notepad = React.createClass({
  render: function() {
    var notepad= this.props.notepad;
    var editor = null;

    var selectedNote = _.find(notepad.notes, function(note) {
      return note.id === notepad.selectedId;
    });

    if(selectedNote) {
      editor = (
        <NoteEditor
          key={selectedNote.id}
          note={selectedNote}
          onChange={this.props.onChangeNote}
          onDelete={this.props.onDeleteNote}>
        </NoteEditor>
      );
    }

    return (
      <div className="notepad">

          <NotesList notepad={notepad} onSelectNote={this.props.onSelectNote} onDelete={this.props.onDeleteNote} onSearch={this.props.onSearchNotes} ></NotesList>


        <div id="note" className="note-body-wrap">
          {editor}
        </div>
      </div>
    );
  }
});

module.exports = Notepad;
