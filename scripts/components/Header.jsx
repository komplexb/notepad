'use strict';

var React = require('react');
window.React = React;

var utils = require('../utils.jsx');

var Header = React.createClass({
  onClickBack: function() {
    utils.updateAppMode("default");
  },

  onClickAdd: function() {
    this.props.onAddNote();
    utils.updateAppMode("editing");
  },

  render: function() {
    var styles = { color: '#288edf' };
    return (
      <div className="topcoat-navigation-bar app-header__wrap">

        <div className="topcoat-navigation-bar__item left">
            <a href="#notepad" title="Back" className="btn-back topcoat-icon-button--quiet x2-top" onClick={this.onClickBack}>
              <span className="topcoat-icon icomatic icon" style={styles}>back</span>
            </a>
        </div>

         <div className="topcoat-navigation-bar__item center">
            <h1 className="topcoat-navigation-bar__title">Notepad</h1>
        </div>

         <div className="topcoat-navigation-bar__item right">

          <a href="#note" title="Add Note" onClick={this.onClickAdd} className="btn--add-note topcoat-icon-button--quiet x2-top">
              <span className="topcoat-icon icomatic icon" style={styles}>plus</span>
            </a>
        </div>

      </div>
    );
  }

});

module.exports = Header;
