'use strict';

var localforage = require('localforage');

localforage.config({
  name: "Notepad App",
  storeName: "notepadDB"
});


var Storage = {
  set: function(key, value) {
    localforage.setItem(key, value, function(err, value) {
      console.log("err: ", err);
//      console.error("An error occurred while saving '%s' to the DB");
    });
  },
  get: function(key) {
    localforage.getItem(key).then(function(value) {
      return value;
    });
  }
}

module.exports = Storage;
